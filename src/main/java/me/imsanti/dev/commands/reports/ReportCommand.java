package me.imsanti.dev.commands.reports;

import me.imsanti.dev.managers.mode.ModeManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReportCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;


        if(args.length == 0) {
            player.sendMessage("syntax correcta: blabla");
            return true;
        }

        switch(args[0].toLowerCase()) {
            case "list":
                if(!(player.hasPermission("sedex.report.list"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                //todo: listar reportes.
                break;
            case "clear":
                if(!(player.hasPermission("sedex.report.clear"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                //todo: limpiar reportes.
                break;

            case "upload":
                if(!(player.hasPermission("sedex.report.upload"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                //todo: subir reporte.
                break;

            case "save":
                if(!(player.hasPermission("sedex.report.save"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                //todo: guardar reporte.
                break;

            case "remove":
                if(!(player.hasPermission("sedex.report.remove"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                //todo: eliminar reporte.
                break;

            default:
                //todo: hacer detecciones para crear un reporte.
        }
        return true;
    }
}