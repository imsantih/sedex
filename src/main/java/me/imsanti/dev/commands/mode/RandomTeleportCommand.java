package me.imsanti.dev.commands.mode;

import me.imsanti.dev.managers.mode.ModeManager;
import me.imsanti.dev.managers.mode.RandomTeleport;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RandomTeleportCommand implements CommandExecutor {

    private final RandomTeleport randomTeleport;

    public RandomTeleportCommand(RandomTeleport randomTeleport) {
        this.randomTeleport = randomTeleport;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(!(player.hasPermission(" sedex.randomteleport"))) {
            player.sendMessage("No permission.");
            return true;
        }

        randomTeleport.randomTeleport(player);
        return true;
    }
}