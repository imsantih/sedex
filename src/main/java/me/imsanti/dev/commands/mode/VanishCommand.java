package me.imsanti.dev.commands.mode;

import me.imsanti.dev.managers.configs.files.MessagesFF;
import me.imsanti.dev.managers.mode.Vanish;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.MessageUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VanishCommand implements CommandExecutor {

    private final Vanish vanish;
    private final MessagesFF messagesFF;

    public VanishCommand(Vanish vanish, MessagesFF messagesFF) {
        this.vanish = vanish;
        this.messagesFF = messagesFF;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(args.length != 1) {
            if(!(player.hasPermission("sedex.vanish.toggle"))) {
                MessageUtils.sendNoPermission(player);
                return true;
            }
            if(vanish.switchVanish(player)) {
                player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("vanish.vanish-enabled")));
                return true;
            }

            player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("vanish.vanish-disabled")));

        }

        switch(args[0].toLowerCase()) {
            case "enable":
                if(!player.hasPermission("sedex.vanish.enable")) {
                    player.sendMessage("No tienes los permisos necesarios");
                    return true;
                }

                vanish.hidePlayerAll(player);
                break;

            case "disable":
                if(!player.hasPermission("sedex.vanish.disable")) {
                    player.sendMessage("No tienes los permisos necesarios");
                    return true;
                }

                vanish.unhidePLayerAll(player);
                break;


            case "toggle":
                if(!player.hasPermission("sedex.vanish.toggle")) {
                    player.sendMessage("No tienes los permisos necesarios");
                    return true;
                }

                vanish.switchVanish(player);
                break;

            default:
                player.sendMessage("usa el comando bien");
                break;
        }

        return true;
    }
}