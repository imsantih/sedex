package me.imsanti.dev.commands.mode;

import me.imsanti.dev.managers.mode.StaffChat;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StaffChatCommand implements CommandExecutor {

    private final StaffChat staffChat;

    public StaffChatCommand(StaffChat staffChat) {
        this.staffChat = staffChat;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(args.length == 0) {
            staffChat.switchStaffChat(player);
            return true;
        }

        switch(args[0].toLowerCase()) {
            case "enable":
                if(!(player.hasPermission("sedex.staffchat.enable"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                staffChat.enableStaffChat(player);
                break;

            case "disable":
                if(!(player.hasPermission("sedex.staffchat.disable"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                staffChat.disableStaffChat(player);
                break;

            case "toggle":
                if(!(player.hasPermission("sedex.staffchat.toggle"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                staffChat.switchStaffChat(player);
                break;

            default:
                staffChat.sendMessage(player, SUtils.toText(SUtils.getSpacesInArgs(args)));
        }

        return true;
    }
}