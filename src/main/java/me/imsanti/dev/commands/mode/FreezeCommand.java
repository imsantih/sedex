package me.imsanti.dev.commands.mode;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.mode.Freeze;
import me.imsanti.dev.utils.PlayerUtils;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class FreezeCommand implements CommandExecutor {

    private final Freeze freeze;
    private final ConfigFile configFile;

    public FreezeCommand(Freeze freeze, ConfigFile configFile) {
        this.freeze = freeze;
        this.configFile = configFile;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(args.length == 0) {
            player.sendMessage("Syntaxis invalida");
            return true;
        }

        switch(args[0].toLowerCase()) {

            case "setfreeze":
                if(!(player.hasPermission("sedex.freeze.setlocation"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                FileConfiguration config = configFile.getConfig();
                SUtils.writeLocation(player.getLocation(), config.createSection("freeze-options.locations.lobbyLocation"));
                configFile.saveConfigFile(config);
                player.sendMessage("Has establecido la localización del Freeze");
                break;

            default:
                if(!(player.hasPermission("sedex.freeze.freeze"))) {
                    player.sendMessage("No permission.");
                    return true;
                }

                if(!PlayerUtils.isValidPlayer(args[0])) {
                    player.sendMessage("Has escrito un jugador invalido");
                    return true;
                }

                freeze.freezePlayer(Bukkit.getPlayer(args[0]), player);
        }
        return true;
    }
}