package me.imsanti.dev.commands.mode;

import me.imsanti.dev.managers.ChatManager;
import me.imsanti.dev.managers.configs.files.MessagesFF;
import me.imsanti.dev.managers.mode.ModeManager;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StaffCommand implements CommandExecutor {

    private final ModeManager modeManager;
    private final MessagesFF messagesFF;

    public StaffCommand(ModeManager modeManager, MessagesFF messagesFF) {
        this.modeManager = modeManager;
        this.messagesFF = messagesFF;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(!(player.hasPermission("sedex.staff.toggle"))) {
            player.sendMessage("No permission.");
            return true;
        }

        if (modeManager.switchStaffMode(player)) {
            player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("staff-mode.staff-enabled")));
            return true;
        }

        player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("staff-mode.staff-disabled")));
        return true;
    }
}