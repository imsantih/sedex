package me.imsanti.dev.commands.chat;

import me.imsanti.dev.managers.ChatManager;
import me.imsanti.dev.utils.PlayerUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatCommand implements CommandExecutor {

    private final ChatManager chatManager;

    public ChatCommand(ChatManager chatManager) {
        this.chatManager = chatManager;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;

        if(args.length != 1) {
            player.sendMessage("Usa el comando bien");
            return true;
        }

        switch(args[0].toLowerCase()) {
            case "enable":
                if(!player.hasPermission("sedex.chat.enable")) {
                    player.sendMessage("No tienes los perimsos reueridos");
                    return true;
                }

                chatManager.enableChat(player);
                break;

            case "disable":
                if(!player.hasPermission("sedex.chat.disable")) {
                    player.sendMessage("No tienes los perimsos reueridos");
                    return true;
                }

                chatManager.disableChat(player);
                break;

            case "toggle":
                if(!player.hasPermission("sedex.chat.toggle")) {
                    player.sendMessage("No tienes los permisos reueridos");
                    return true;
                }

                chatManager.toggleChat(player);
                break;

            case "clear":
                if(!player.hasPermission("sedex.chat.clear")) {
                    player.sendMessage("No tienes los permisos reueridos");
                    return true;
                }

                chatManager.clearChat(player);
                break;

            default:
                player.sendMessage("usa el comando bien");
                break;
        }

        return true;
    }
}