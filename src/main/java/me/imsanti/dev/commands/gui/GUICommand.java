package me.imsanti.dev.commands.gui;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.configs.ConfigManager;
import me.imsanti.dev.managers.configs.files.GUIFile;
import me.imsanti.dev.managers.gui.EditGUIManager;
import me.imsanti.dev.managers.gui.GUIUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GUICommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Este comando solo puede ser usado por jugadores");
            return true;
        }
        Player player = (Player) sender;


        if(args.length == 0) {
            player.sendMessage("comando mal usado");
            return true;
        }

        switch(args[0].toLowerCase()) {
            case "open":
                String guiName = args[1];

                GUIUtils guiUtils = new GUIUtils(new EditGUIManager(new GUIFile(new ConfigManager())), player, new GUIFile(new ConfigManager()), new ConfigFile(new ConfigManager()));

                guiUtils.createGUI(EditGUIManager.GUIType.valueOf(guiName), player);
                break;

            case "edit":
                String guiName2 = args[1];
                EditGUIManager editGUIManager = new EditGUIManager(new GUIFile(new ConfigManager()));

                editGUIManager.openGUIEdit(player, guiName2);
                break;

            default:
                player.sendMessage("usa bien el comando");
        }
        return true;
    }
}