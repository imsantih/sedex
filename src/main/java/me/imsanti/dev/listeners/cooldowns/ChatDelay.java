package me.imsanti.dev.listeners.cooldowns;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.objects.Cooldown;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatDelay implements Listener {

    private final Cooldown chatCooldown;

    public ChatDelay(Cooldown chatCooldown) {
        this.chatCooldown = chatCooldown;
    }

    @EventHandler
    private void handleChatCooldown(AsyncPlayerChatEvent event) {
        Bukkit.broadcastMessage("se llamo al evento :D");
        if(!(chatCooldown.isInCooldown(event.getPlayer().getUniqueId()))) {
            chatCooldown.setCooldown(event.getPlayer().getUniqueId());
            return;
        }

        event.setCancelled(true);
        event.getPlayer().sendMessage("Todavía estas en Cooldown ( " + chatCooldown.getRemainingCooldown(event.getPlayer().getUniqueId()) + ")");
    }

    public void shutdownMe() {
        AsyncPlayerChatEvent.getHandlerList().unregister(new ChatDelay(chatCooldown));

        Bukkit.broadcastMessage("disabled!");
    }
}
