package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.ChatManager;
import me.imsanti.dev.managers.mode.Freeze;
import me.imsanti.dev.managers.mode.StaffChat;
import me.imsanti.dev.managers.objects.Cooldown;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    private final StaffChat staffChat;
    private final Freeze freeze;
    private final ChatManager chatManager;

    public ChatListener(StaffChat staffChat, Freeze freeze, ChatManager chatManager) {
        this.freeze = freeze;
        this.staffChat = staffChat;
        this.chatManager = chatManager;
    }

    @EventHandler(ignoreCancelled = true)
    private void HandleStaffChat(AsyncPlayerChatEvent event)  {
        if(!(staffChat.getStaffChatPlayers().contains(event.getPlayer().getUniqueId()))) return;

        event.setCancelled(true);
        staffChat.sendMessage(event.getPlayer(), event.getMessage());
    }

    @EventHandler(ignoreCancelled = true)
    private void handleFreezeChat(AsyncPlayerChatEvent event) {
        if(!(freeze.isFrozed(event.getPlayer()))) return;

        event.setCancelled(true);

        Player staff = Bukkit.getPlayer(freeze.getPlayerFreezeStaff().get(event.getPlayer().getUniqueId()));
        Player frozed = event.getPlayer();

        if(staff == null || !staff.isOnline()) Bukkit.getConsoleSender().getServer().dispatchCommand(Bukkit.getConsoleSender(), "ban " + frozed.getName() + "Logout in SS");

        staff.sendMessage(ColorUtils.color("&cFreeze Chat: &e" + frozed.getName() + " &7: &f" + event.getMessage()));
    }

    @EventHandler
    private void handleChatStatus(AsyncPlayerChatEvent event) {
        if(chatManager.isChatEnabled()) return;

        Player player = event.getPlayer();
        if(player.hasPermission("sedex.chat.bypass")) return;
        if(freeze.isFrozed(player)) return;
        if(staffChat.isInStaffChat(player)) return;

        event.setCancelled(true);
    }

}
