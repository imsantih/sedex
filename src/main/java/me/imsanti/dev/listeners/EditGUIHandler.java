package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.gui.EditGUIManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class EditGUIHandler implements Listener {

    private final EditGUIManager editGUIManager;
    public EditGUIHandler(EditGUIManager editGUIManager) {
        this.editGUIManager = editGUIManager;
    }
    @EventHandler
    private void handleFinishEdit(InventoryCloseEvent event) {
        if(!(editGUIManager.getPlayerEditGUI().containsKey(event.getPlayer().getUniqueId())) || !event.getInventory().getTitle().contains("Editing")) return;

        editGUIManager.saveGUIContents(event.getInventory(), editGUIManager.getPlayerEditGUI().get(event.getPlayer().getUniqueId()));
    }

    @EventHandler(priority = EventPriority.HIGH)
    private void handleClick(InventoryClickEvent event) {
        if(!(editGUIManager.getPlayerEditGUI().containsKey(event.getWhoClicked().getUniqueId())) || !event.getInventory().getTitle().contains("Editing")) return;

        event.setCancelled(false);

    }

}
