package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.configs.files.MessagesFF;
import me.imsanti.dev.managers.mode.Freeze;
import me.imsanti.dev.managers.mode.ModeManager;
import me.imsanti.dev.managers.mode.Vanish;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.MessageUtils;
import me.imsanti.dev.utils.PlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class ConnectionListener implements Listener {

    private final Freeze freeze;
    private final MessagesFF messagesFF;
    private final Vanish vanish;
    private final ConfigFile configFile;
    private final ModeManager modeManager;

    public ConnectionListener(Freeze freeze, Vanish vanish, ConfigFile configFile, ModeManager modeManager, MessagesFF messagesFF) {
        this.freeze = freeze;
        this.vanish = vanish;
        this.configFile = configFile;
        this.modeManager = modeManager;
        this.messagesFF = messagesFF;
    }


    @EventHandler
    private void handleStaffJoinMessage(final PlayerJoinEvent event) {
        if(!event.getPlayer().hasPermission("sedex.staff")) return;

        PlayerUtils.sendGlobalMessage("sedex.message.staffJoin", ColorUtils.color(messagesFF.getMessages().getString("staff-alerts.staff-join")).replaceAll("%player%", event.getPlayer().getName()));
    }

    @EventHandler
    private void uhandleStaffLeftMessage(final PlayerQuitEvent event) {
        if(!event.getPlayer().hasPermission("sedex.staff")) return;

        PlayerUtils.sendGlobalMessage("sedex.message.staffLeave", ColorUtils.color(messagesFF.getMessages().getString("staff-alerts.staff-leave").replaceAll("%player%", event.getPlayer().getName())));
    }

    @EventHandler
    private void handleFreezeDisconnect(PlayerQuitEvent event) {
        if(!(freeze.isFrozed(event.getPlayer()))) return;

        MessageUtils.sendAllStaffMessage("&cEl jugador" + event.getPlayer().getName() + " se desconecto frozeado.");
        freeze.getFreezedPlayers().remove(event.getPlayer().getUniqueId());
        freeze.getPlayerFreezeStaff().remove(event.getPlayer().getUniqueId());

    }

    @EventHandler
    private void handleVanish(PlayerJoinEvent event) {
        for(UUID uuid : vanish.getHidedPlayers()) {
            Player player = Bukkit.getPlayer(uuid);
            if(event.getPlayer().hasPermission("sedex.staff")) return;

            vanish.hidePlayer(player, event.getPlayer());
        }
    }
    @EventHandler
    private void handleStaffMode(PlayerJoinEvent event) {
        if(!(event.getPlayer().hasPermission("sedex.staff"))) return;

        if(configFile.getConfig().getBoolean("mode-options.mode-onJoin")) modeManager.enableStaffMode(event.getPlayer());
        if(configFile.getConfig().getBoolean("mode-options.vanish-onJoin")) vanish.hidePlayerAll(event.getPlayer());

    }
}
