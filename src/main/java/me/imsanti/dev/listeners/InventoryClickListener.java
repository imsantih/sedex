package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.gui.EditGUIManager;
import me.imsanti.dev.managers.gui.GUIManager;
import me.imsanti.dev.managers.gui.GUIUtils;
import me.imsanti.dev.managers.mode.ModeUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.IOException;
import java.util.UUID;

public class InventoryClickListener implements Listener {

    private final ModeUtils modeUtils;
    private final EditGUIManager editGUIManager;
    private final GUIUtils guiUtils;

    public InventoryClickListener(ModeUtils modeUtils, EditGUIManager editGUIManager, GUIUtils guiUtils) {
        this.modeUtils = modeUtils;
        this.editGUIManager = editGUIManager;
        this.guiUtils = guiUtils;
    }
    @EventHandler
    private void handleClickEvent(final InventoryClickEvent event) throws IOException {
        if (!(event.getWhoClicked() instanceof Player)) return;
        if(editGUIManager.getPlayerEditGUI().containsKey(event.getWhoClicked().getUniqueId()) && event.getInventory().getTitle().contains("Editing")) return;
        Player player = (Player) event.getWhoClicked();
        UUID playerUUID = player.getUniqueId();

        UUID inventoryUUID = GUIManager.openInventories.get(playerUUID);
        if (inventoryUUID == null) return;
        if(event.getInventory().getTitle().equalsIgnoreCase("Editando GUI")) return;


        event.setCancelled(true);
        GUIManager gui = GUIManager.getInventoriesByUUID().get(inventoryUUID);
        GUIManager.setupGUIAction action = gui.getActions().get(event.getSlot());
        if (action == null) return;

        action.click(player);

    }

    @EventHandler
    private void handModeMove(InventoryClickEvent event) {
        if(!(event.getWhoClicked() instanceof Player)) return;
        if(editGUIManager.getPlayerEditGUI().containsKey(event.getWhoClicked().getUniqueId()) && event.getInventory().getTitle().contains("Editing")) return;
        Player player = (Player) event.getWhoClicked();
        if(modeUtils.isInMode(player)) event.setCancelled(true);
    }

    @EventHandler
    private void onInventoryClose(InventoryCloseEvent event) {
        GUIManager.openInventories.remove(event.getPlayer().getUniqueId());

    }

    @EventHandler
    private void onQuit(PlayerQuitEvent event) {
        GUIManager.openInventories.remove(event.getPlayer().getUniqueId());
        guiUtils.getPlayerPage().remove(event.getPlayer().getUniqueId());
    }

}