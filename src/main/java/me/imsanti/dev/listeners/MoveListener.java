package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.mode.Freeze;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {

    private final Freeze freeze;

    public MoveListener(Freeze freeze) {
        this.freeze = freeze;
    }

    private void handleFreezeMove(final PlayerMoveEvent event) {
        if(!(freeze.isFrozed(event.getPlayer()))) return;

        Player player = event.getPlayer();
        player.teleport(event.getFrom());

    }

}
