package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.mode.ModeManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class DamageListener implements Listener {

    private final ModeManager modeManager;
    private final ConfigFile configFile;
    public DamageListener(ModeManager modeManager, ConfigFile configFile) {
        this.modeManager = modeManager;
        this.configFile = configFile;
    }
    @EventHandler
    private void handleModeDamage(final EntityDamageEvent event) {
        if(!(event.getEntity() instanceof Player)) return;

        Player player = (Player) event.getEntity();
        if(modeManager.isInStaffMode(player) && configFile.getConfig().getBoolean("mode-options.protections.noDamage")) event.setCancelled(true);
    }
}
