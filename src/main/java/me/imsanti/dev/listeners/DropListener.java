package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.mode.ModeManager;
import me.imsanti.dev.managers.mode.ModeUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class DropListener implements Listener {

    private final ModeManager modeManager;
    private final ModeUtils modeUtils;

    public DropListener(ModeUtils modeUtils, ModeManager modeManager) {
        this.modeUtils = modeUtils;
        this.modeManager = modeManager;
    }
    @EventHandler
    private void on(PlayerDropItemEvent event) {
        if(!(modeUtils.isInMode(event.getPlayer()))) return;

        if(modeManager.getItemData().containsKey(event.getItemDrop().getItemStack())) event.setCancelled(true);
    }

}
