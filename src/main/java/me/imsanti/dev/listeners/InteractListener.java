package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.mode.Freeze;
import me.imsanti.dev.managers.mode.ModeManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class InteractListener implements Listener {

    private final Freeze freeze;
    private final ModeManager modeManager;
    private final ConfigFile configFile;
    public InteractListener(Freeze freeze, ModeManager modeManager, ConfigFile configFile) {
        this.freeze = freeze;
        this.modeManager = modeManager;
        this.configFile = configFile;
    }

    @EventHandler
    private void handleProtections(final PlayerInteractEvent event) {
        FileConfiguration config = configFile.getConfig();

        if(freeze.isFrozed(event.getPlayer()) && config.getBoolean("freeze-options.protections.noInteract")) event.setCancelled(true);
        if(modeManager.isInStaffMode(event.getPlayer()) && config.getBoolean("mode-options.protections.noInteract")) event.setCancelled(true);
    }

    @EventHandler
    private void handleStaffClick(final PlayerInteractEvent event) {
        if(!(modeManager.isInStaffMode(event.getPlayer()))) return;

        if(!(modeManager.getItemData().containsKey(event.getPlayer().getItemInHand()))) return;

        modeManager.handleAction(modeManager.getItemData().get(event.getPlayer().getItemInHand()), event.getPlayer());
        
    }

    @EventHandler
    private void handleFreezeClick(PlayerInteractEntityEvent event) {
        if(!(event.getRightClicked() instanceof Player)) return;
        if(!(modeManager.isInStaffMode(event.getPlayer()))) return;
        if(!(modeManager.getItemData().containsKey(event.getPlayer().getItemInHand()))) return;

        if(!modeManager.getItemData().get(event.getPlayer().getItemInHand()).equals("FREEZE")) return;

        freeze.switchFreeze((Player) event.getRightClicked(), event.getPlayer());
    }
}
