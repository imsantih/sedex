package me.imsanti.dev.listeners;

import me.imsanti.dev.managers.configs.files.LocationsFile;
import me.imsanti.dev.managers.mode.Freeze;
import me.imsanti.dev.managers.mode.Vanish;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import java.util.Objects;

public class WorldChangeListener implements Listener {

    private final Freeze freeze;
    private final LocationsFile locationsFile;
    private final Vanish vanish;

    public WorldChangeListener(Freeze freeze, LocationsFile locationsFile, Vanish vanish) {
        this.locationsFile = locationsFile;
        this.freeze = freeze;
        this.vanish = vanish;
    }
    @EventHandler
    private void on(PlayerChangedWorldEvent event) {
        if(!(freeze.isFrozed(event.getPlayer()))) return;

        event.getPlayer().teleport(SUtils.readLocation(Objects.requireNonNull(locationsFile.getLocationsConfg().getConfigurationSection("freezeLocation"))));

    }

    @EventHandler
    private void handleFixVanish(PlayerChangedWorldEvent event) {
        if(!(vanish.isVanish(event.getPlayer()))) return;

        vanish.unhideHim(event.getPlayer());
    }
}
