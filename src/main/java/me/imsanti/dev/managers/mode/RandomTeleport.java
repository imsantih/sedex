package me.imsanti.dev.managers.mode;


import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.configs.files.MessagesFF;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class RandomTeleport {

    private final ConfigFile configFile;
    private final MessagesFF messagesFF;

    public RandomTeleport(ConfigFile configFile, MessagesFF messagesFF) {
        this.configFile = configFile;
        this.messagesFF = messagesFF;
    }
    @SuppressFBWarnings("DM_NEXTINT_VIA_NEXTDOUBLE")
    public void randomTeleport(Player player) {
        Bukkit.broadcastMessage("el código se ejecuto!");
        if(Bukkit.getOnlinePlayers().size() < configFile.getConfig().getInt("random-teleport.min-players")) {
            player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("random-teleport.insufficient-players")));
            return;
        }


        ArrayList<Player> listPlayers = new ArrayList<>(Bukkit.getOnlinePlayers());

        if(configFile.getConfig().getInt("random-teleport.min-players") <= 1 && Bukkit.getOnlinePlayers().size() == 1) {
            if(!listPlayers.contains(player)) listPlayers.add(player);
        }else {
            listPlayers.remove(player);
        }

        int i = 0;
        Player randomPlayer = listPlayers.stream().skip((int) (listPlayers.size() * Math.random())).findFirst().orElse(null);
        while(randomPlayer == null && i < 5) {
           randomPlayer = listPlayers.stream().skip((int) (listPlayers.size() * Math.random())).findFirst().orElse(null);
            i++;
        }

        if(randomPlayer == null) {
            player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("random-teleport.no-player-found")));
            return;
        }

        player.teleport(randomPlayer.getLocation());
        player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("random-teleported")).replace("%player%", randomPlayer.getName()));
        listPlayers = null;
        randomPlayer = null;
    }

}
