package me.imsanti.dev.managers.mode;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.gui.guis.StaffOnlineGUI;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.MessageUtils;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ModeManager {

    public ArrayList<UUID> playersInStaffMode = new ArrayList<>();
    private final HashMap<ItemStack, String> itemData = new HashMap<>();
    private final HashMap<ItemStack, Integer> modeItems = new HashMap<>();

    private final MessageUtils messageUtils;
    private final ModeUtils modeUtils = new ModeUtils(this);
    private final ConfigFile configFile;
    private final Freeze freeze;
    private final Vanish vanish;
    private final RandomTeleport randomTeleport;
    public ModeManager(MessageUtils messageUtils, ConfigFile configFile, RandomTeleport randomTeleport, Freeze freeze, Vanish vanish) {
        this.messageUtils = messageUtils;
        this.configFile = configFile;
        this.randomTeleport = randomTeleport;
        this.freeze = freeze;
        this.vanish = vanish;
    }

    public boolean switchStaffMode(Player player) {
        if(modeUtils.isInMode(player)) {
            disableStaffMode(player);
            return false;
        }
        enableStaffMode(player);
        return true;
    }

    public void enableStaffMode(Player player ) {
        FileConfiguration config = configFile.getConfig();
        if(config.getBoolean("mode-options.clear-Inventory")) player.getInventory().clear();

        player.setGameMode(GameMode.valueOf(config.getString("mode-options.gamemode")));
        player.setAllowFlight(config.getBoolean("mode-options.enableFly"));

        playersInStaffMode.add(player.getUniqueId());
        giveItems(player);
        return;
    }


    public void disableStaffMode(Player player) {
        player.getInventory().clear();
        playersInStaffMode.remove(player.getUniqueId());
    }

    public ArrayList<UUID> getPlayersInStaffMode() {
        return playersInStaffMode;
    }

    private void giveItems(Player player) {
          for(ItemStack item : modeItems.keySet()) {
              player.getInventory().setItem(modeItems.get(item), item);

          }
    }

    public void loadItems() {
        FileConfiguration config = configFile.getConfig();
        ConfigurationSection section = config.getConfigurationSection("mode-items");
        for(String key : section.getKeys(false)) {
            ConfigurationSection sectionItem = config.getConfigurationSection("mode-items." + key);
            if(sectionItem == null) continue;

            itemData.put(SUtils.readItem(sectionItem), sectionItem.getString("action"));
            modeItems.put(SUtils.readItem(sectionItem), sectionItem.getInt("slot"));
            Bukkit.getLogger().info("[Item-Manager] The Item " + sectionItem.getName() + " was successfully loaded.");
        }
    }

    public boolean isInStaffMode(Player player) {
        return getPlayersInStaffMode().contains(player.getUniqueId());
    }

    public void handleAction(String action, Player player) {
        switch(action.toUpperCase()) {
            case "RANDOM-TP":
                randomTeleport.randomTeleport(player);
                break;

            case "VANISH":
                vanish.switchVanish(player);
                break;

            case "STAFF-LIST":
                StaffOnlineGUI gui = new StaffOnlineGUI();
                gui.openInventory(player);
                break;
            default:
                break;
        }
    }

    public HashMap<ItemStack, String> getItemData() {
        return itemData;
    }
}
