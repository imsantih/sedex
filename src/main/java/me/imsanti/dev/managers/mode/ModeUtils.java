package me.imsanti.dev.managers.mode;

import org.bukkit.entity.Player;

public class ModeUtils {
    private final ModeManager modeManager;

    public ModeUtils(ModeManager modeManager) {
        this.modeManager = modeManager;
    }

    public boolean isInMode(Player player) {
        return modeManager.getPlayersInStaffMode().contains(player.getUniqueId());
    }

    public static boolean isStaff(Player player) {
        return player.hasPermission("sedex.staff");

    }
}
