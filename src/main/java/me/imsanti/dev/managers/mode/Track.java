package me.imsanti.dev.managers.mode;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class Track {

    private final HashMap<UUID, UUID> tracking = new HashMap<>();

    public void enableTracking(Player player, Player target) {
        tracking.put(player.getUniqueId(), target.getUniqueId());
        player.sendMessage("Ahora estas trackeando al jugador " + target.getName());
    }

    public void disableTracking(Player player) {

        player.sendMessage("Ahora ya no estas trackeando a " + Bukkit.getPlayer(tracking.get(player.getUniqueId())).getName());
        tracking.remove(player.getUniqueId());
    }

    public Player getTracked(Player player) {
        return Bukkit.getPlayer(tracking.get(player.getUniqueId()));
    }

}
