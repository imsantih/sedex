package me.imsanti.dev.managers.mode;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class Vanish {

    private final ArrayList <UUID> hidedPlayers = new ArrayList<>();

    public void hidePlayerAll(Player player) {
        hidedPlayers.add(player.getUniqueId());
        for (Player target: Bukkit.getServer().getOnlinePlayers()) {
            if(target.hasPermission("sedex.staff")) continue;

            if(target.getUniqueId() == player.getUniqueId()) continue;

            target.hidePlayer(player);

        }
    }

    public void unhidePLayerAll(Player player) {
        hidedPlayers.remove(player.getUniqueId());
        for (Player target: Bukkit.getServer().getOnlinePlayers()) {
            target.showPlayer(player);

        }
    }

    public boolean switchVanish(Player player) {
        if(isVanish(player)) {
            unhidePLayerAll(player);
            return false;
        }
        hidePlayerAll(player);
        return true;
    }

    public void unhideHim(Player player) {
        player.showPlayer(player);
    }

    public boolean isVanish(Player player) {
        return hidedPlayers.contains(player.getUniqueId());
    }

    public void hidePlayer(Player player, Player target) {
        target.hidePlayer(player);
    }

    public ArrayList < UUID > getHidedPlayers() {
        return hidedPlayers;
    }
}