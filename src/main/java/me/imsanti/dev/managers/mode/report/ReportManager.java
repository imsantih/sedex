package me.imsanti.dev.managers.mode.report;

import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.Hastebin;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ReportManager {

    public final static HashMap<UUID, Integer> playerReportNumber = new HashMap<>();

    public void doReport(Player reported, String reason, Player reporter) {

        LocalDateTime now = LocalDateTime.now();

        int reportPreNUmber = playerReportNumber.getOrDefault(reported.getUniqueId(), 0);
        playerReportNumber.put(reported.getUniqueId(), reportPreNUmber + 1);

        int reportNumber = playerReportNumber.get(reported.getUniqueId());

        int totalReports = Report.getPlayerTotalReports().getOrDefault(reported.getUniqueId(), 0) + 1;
        Report newReport = new Report(reporter, reported, reason, String.valueOf(now), reportNumber, UUID.randomUUID(), totalReports);
        newReport.createReport();

        reporter.sendMessage("Has creado el Reporte" + newReport.getReportNumber());

//        reportedPlayers.add(player.getUniqueId());
//
//        String formatedReason =
//        reporter.sendMessage(ColorUtils.color("&aTu reporte ha sido enviado con exito."));
//        reporter.sendMessage(ColorUtils.color("&aUn staff lo revisara pronto."));
//
//        for(Player player1 : Bukkit.getOnlinePlayers()) {
//            if(!(ModeUtils.isStaff(player1))) return;
//
//            player1.sendMessage("");
//            player.sendMessage(ColorUtils.color("&6&lNuevo Reporte"));
//            player.sendMessage("");
//            player1.sendMessage(ColorUtils.color("&eRazon: &b " + reason));
//            player1.sendMessage(ColorUtils.color("&fReportante: &b " + reporter.getName()));
//
//            Bukkit.broadcastMessage(playerReportReason.get(player.getUniqueId()));
//            SUtils.playSound(player1, Sound.valueOf("NOTE_PLING"));
//        }
    }

    public void teleportToReport(Player reporter, Player staff) {
        staff.teleport(reporter.getLocation());
        staff.sendMessage(ColorUtils.color("6aHas sido teletransportado a " + reporter.getName()));
    }


    public String uploadReports(Player target) throws IOException {
        ArrayList<String> reportInfo = new ArrayList<>();

        for(int i = 0; i <= Report.getReportCount(target); i++) {
            Report foundReport = Report.getPlayerReports(target).get(i);
            if(foundReport == null) continue;
            reportInfo.add("[" + foundReport.getDataFormatted() + "-" + foundReport.getReportNumber() + "]"
                    + " " + foundReport.getReporter().getName() + " reported " + foundReport.getReported().getName() + " " + "(" + i + ")" + " for " + foundReport.getReason());
        }

        return Hastebin.createHasteBin(reportInfo);
    }

    //todo: save report
    public void saveReport(Player player) {

    }
}


