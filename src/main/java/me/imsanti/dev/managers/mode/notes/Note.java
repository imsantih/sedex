package me.imsanti.dev.managers.mode.notes;

import me.imsanti.dev.managers.configs.ConfigManager;
import me.imsanti.dev.managers.configs.files.DataFile;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.*;

public class Note {

    private final String reportTime;
    private final UUID targetUUID;
    private final UUID staffUUID;
    private final String content;
    private final String currentValue;
    private int listNumber;

    private static final ArrayList<Note> notes = new ArrayList<>();

    private static final HashMap<UUID, String> notesDB = new HashMap<>();

    public Note(String reportTime, UUID targetUUID, UUID staffUUID, String content, int listNumber, String currentValue) {
        this.reportTime = reportTime;
        this.targetUUID = targetUUID;
        this.staffUUID = staffUUID;
        this.content = content;
        this.listNumber = listNumber;
        this.currentValue = currentValue;
    }

    public void create() {
        Player player = Bukkit.getPlayer(staffUUID);
        DataFile dataFile = new DataFile(new ConfigManager(), player);
        dataFile.createDataFile();

        FileConfiguration config = DataFile.getDataFile(player);

        ConfigurationSection noteSection = config.createSection("notes." + UUID.randomUUID().toString());
        ArrayList<String> writeNoteConfig = new ArrayList<>();
        writeNoteConfig.add("Note Time: " + reportTime);
        writeNoteConfig.add("Player " + Bukkit.getPlayer(targetUUID).getName());
        writeNoteConfig.add("Staff " + Bukkit.getPlayer(staffUUID).getName());
        writeNoteConfig.add("Content " + content);

        noteSection.set("Date:", reportTime);
        noteSection.set("Player:", Bukkit.getPlayer(targetUUID).getName());
        noteSection.set("Staff:", Bukkit.getPlayer(staffUUID).getName());
        noteSection.set("Content:", content);
        dataFile.saveDataFile(config);

        //notesDB.put(targetUUID, currentValue + formatNote());
        notes.add(this);
    }

    public ArrayList<String> list() {
        if (!(notesDB.containsKey(targetUUID))) return null;
        ArrayList<String> allNotes = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            if(allNotes.contains(Arrays.toString(notesDB.get(targetUUID).split("Split!" + i + "!")).replace("[", "").replace("]", "").replace("[]", ""))) return allNotes;

            if(!(allNotes.size() == 0)) allNotes.remove(0);

            if (!(Arrays.toString(notesDB.get(targetUUID).split("Split!" + i + "!")).contains(Bukkit.getPlayer(staffUUID).getName())))
                return allNotes;

            allNotes.add(Arrays.toString(notesDB.get(targetUUID).split("Split!" + i + "!")).replace("[", "").replace("]", "").replace("[]", ""));
        }

        return allNotes;
    }

    public static Note getNote(Player player) {
        for (Note note : notes) {
            if(note.getStaffUUID().equals(player.getUniqueId())) return note;
        }
        return null;
    }

    private String formatNote() {
        return "Split!" + listNumber + "!" + Bukkit.getPlayer(targetUUID).getName() + ":" + Bukkit.getPlayer(staffUUID).getName() + ":" + content + ":" + reportTime + "Split!" + listNumber + "!";
    }

    public ArrayList<String> splitNotes() {
        if((!(notesDB.containsKey(targetUUID)))) return null;

        StringBuilder checkFinish = new StringBuilder();
        int j = 0;
        ArrayList<String> results = new ArrayList<>();
        while(!(checkFinish.toString().contains(String.valueOf(j)))){
            for(int i = 0; i < 500; i++) {
                String formatted = "";
                String[] split = notesDB.get(targetUUID).split("Split!" + i + "!");

                String[] split2 = Arrays.toString(split).split(":");
                formatted = split2[0] + "=" + split2[1] + "=" + split2[2] + "=";
                formatted = formatted.replace("[", "").replace(",", "").trim();


                results.add(formatted);
                checkFinish.append(i);

            }
        }


        return results;
    }

    public String getReportTime() {
        return reportTime;
    }

    public String getContent() {
        return content;
    }

    public int getListNumber() {
        return listNumber;
    }

    public UUID getStaffUUID() {
        return staffUUID;
    }

    public UUID getTargetUUID() {
        return targetUUID;
    }

    public static HashMap<UUID, String> getNotesDB() {
        return notesDB;
    }
}
