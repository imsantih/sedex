package me.imsanti.dev.managers.mode.notes;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class NotesManager {

    public void createNotes(Player player, Player staff, String reason, String reportTime, int notesNumber) {
        new Note(reportTime, player.getUniqueId(), staff.getUniqueId(), reason, notesNumber, Note.getNotesDB().getOrDefault(player.getUniqueId(), "")).create();
        Bukkit.broadcastMessage("nota creada");
    }

    public ArrayList<String> listNotes(Note note) {
        return note.splitNotes();
    }
}
