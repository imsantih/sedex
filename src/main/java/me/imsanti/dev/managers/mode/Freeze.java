package me.imsanti.dev.managers.mode;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.configs.files.LocationsFile;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.*;

public class Freeze {

    private final ConfigFile configFile;

    private ArrayList<UUID> freezedPlayers = new ArrayList<>();
    private Map<UUID, UUID> playerFreezeStaff = new HashMap<>();
    public Freeze(LocationsFile locationsFile, ConfigFile configFile) {
        this.configFile = configFile;
    }


    public void freezePlayer(Player player, Player executor) {
        FileConfiguration config = configFile.getConfig();
        freezedPlayers.add(player.getUniqueId());
        player.sendMessage("Has sido frozeado por + " + executor.getName());

        playerFreezeStaff.put(player.getUniqueId(), executor.getUniqueId());

        handleModes(player);

        for(Player player1 : Bukkit.getOnlinePlayers()) {
            if(!(ModeUtils.isStaff(player1))) return;

            player1.sendMessage("El staff" + executor.getName() + " frezeo a " + player.getName());
        }
    }

    public void unfreezePlayer(Player player) {
        freezedPlayers.remove(player.getUniqueId());
        player.sendMessage("Ya no estas frozeado");
    }


    public void banFreezedPlayers() {
        for(UUID uuid : freezedPlayers) {
            Player player = Bukkit.getPlayer(uuid);
            Bukkit.getConsoleSender().getServer().dispatchCommand(Bukkit.getConsoleSender(), "ban " + player.getName() + " Desconectarse en SS");
            Bukkit.getLogger().info("El jugador " + player.getName() + "ha sido baneado por estar frozeado en un reinicio.");
        }
    }

    public void switchFreeze(Player player, Player executor) {
        if(isFrozed(player)) {
            unfreezePlayer(player);
            return;
        }

        freezePlayer(player, executor);

    }

    public void handleModes(Player player) {
        FileConfiguration config = configFile.getConfig();
        if(getFreezeMode().equalsIgnoreCase("LOBBY")) {
            player.teleport(SUtils.readLocation(config.getConfigurationSection("freezeLocation")));
        }

    }

    public boolean isFrozed(Player player) {
        return freezedPlayers.contains(player.getUniqueId());
    }

    public ArrayList<UUID> getFreezedPlayers() {
        return freezedPlayers;
    }

    public Map<UUID, UUID> getPlayerFreezeStaff() {
        return playerFreezeStaff;
    }

    public String getFreezeMode() {
        if(configFile.getConfig().getString("freeze-options.mode").equalsIgnoreCase("LOBBY")) return "LOBBY";

        return "LOCATION";

    }
}
