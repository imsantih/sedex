package me.imsanti.dev.managers.mode.report;

import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.io.File;

public class Report {

    private final Map<UUID, UUID> reportUUID = new HashMap<>();
    private final Map<UUID, String> reportReason = new HashMap<>();
    private final Map<UUID, String> reportTime = new HashMap<>();
    private final Map<UUID, UUID> reporterUUID = new HashMap<>();
    private final Map<UUID, Integer> reportNumberList = new HashMap<>();
    private final Map<UUID, Integer> playerSaveReportNumber = new HashMap<>();
    private static final Map<UUID, Integer> playerTotalReports = new HashMap<>();

    private static final ArrayList<Report> reports = new ArrayList<>();

    private final Player reporter;
    private final Player reported;
    private final String reason;
    private final String dataFormatted;
    private final int reportNumber;

    private final ConfigManager configManager;

    public Report(Player reporter, Player reported, String  reason, String time, int reportNumber, UUID reportUUID, int totalReports) {
        this.reporter = reporter;
        this.reported = reported;
        this.reason = reason;
        this.dataFormatted = time;
        this.reportNumber = reportNumber;
        this.configManager = new ConfigManager();
    }
    public void createReport() {
        UUID reportUUID = UUID.randomUUID();
        this.reporterUUID.put(reporter.getUniqueId(), reportUUID);
        this.reportUUID.put(reported.getUniqueId(), reportUUID);
        this.reportReason.put(reported.getUniqueId(), reason);
        this.reportTime.put(reported.getUniqueId(), dataFormatted);
        this.reportNumberList.put(reported.getUniqueId(), reportNumber);
        this.reportUUID.put(reported.getUniqueId(), reportUUID);
        int totalReports = playerTotalReports.getOrDefault(reported.getUniqueId(), 0) + 1;
        playerTotalReports.put(reported.getUniqueId(), totalReports);
        reports.add(new Report(reporter, reported, reason, dataFormatted, reportNumber, reportUUID, totalReports));
    }

    public int getReportNumberFromConfig(Player player, FileConfiguration config) {
        for(int i = 0; i < 18;) {
            if(config.getConfigurationSection("reports-" + i) == null) return i;
            i++;
        }
        return 0;
    }


    public Player getReported() {
        return reported;
    }

    public Player getReporter() {
        return reporter;
    }

    public String getDataFormatted() {
        return dataFormatted;
    }

    public String getReason() {
        return reason;
    }

    public int getReportNumber() {
        return reportNumber;
    }

    public static Report getReport(Player player, int reportNumber) {
        for(Report report : reports) {
            if(report.getReported().getUniqueId().equals(player.getUniqueId()) && (report.getReportNumber() == reportNumber)) return report;
        }
        return null;
    }

    public void deleteReport(Player player, int reportNumber) {
        Report deleteReport = getReport(player, reportNumber);
        reports.remove(deleteReport);
    }

    public static int getReportCount(Player player) {
        return Integer.parseInt(playerTotalReports.getOrDefault(player.getUniqueId(), 0).toString());
    }

    public static ArrayList<Report> getPlayerReports(Player player) {
        ArrayList<Report> allReports = new ArrayList<>();
        for(int i = 0; i <= getReportCount(player); i++) {
            Report report = getReport(player, i);
            allReports.add(report);
        }
        return allReports;
    }

    public void saveReport() {
        String location = "plugins/Sedex/reports";
        String name = reported.getName();
        File file = new File(location, name + ".yml");
        if(!(file.exists())) {
            configManager.createConfigFile(location, name);

            playerSaveReportNumber.put(reported.getUniqueId(), playerSaveReportNumber.getOrDefault(reported.getUniqueId(), 0) + 1);
            FileConfiguration config = configManager.getConfigFromFile(location, name);
            writeSection(config.createSection("reports-" + playerSaveReportNumber.get(reported.getUniqueId())));
            configManager.saveConfigFile(file, config);
            return;
        }

        FileConfiguration config = configManager.getConfigFromFile(location, name);
        writeSection(config.createSection("reports-" + reportNumber));
        configManager.saveConfigFile(file, config);

    }

    public static void readReports() {
        File[] files = new File("plugins/Sedex/reports").listFiles();
        if(files == null) return;
        ConfigManager configManager = new ConfigManager();
        for (File file : files) {
            if (!file.isFile()) continue;
            if(!(file.getName().contains(".yml"))) return;

            FileConfiguration config = configManager.getConfigFromFile("plugins/Sedex/reports", file.getName().replace(".yml", ""));
            for(int i = 1; i <= 15; i++) {
                if(config.getConfigurationSection("reports-" + i) == null) continue;
                Bukkit.broadcastMessage("Numero leido por el FillReports: " + i);
                ConfigurationSection section = config.getConfigurationSection("reports-" + i);

                Player reporter = Bukkit.getPlayer(UUID.fromString(section.getString("reporter-UUID")));
                Player reported = Bukkit.getPlayer(UUID.fromString(section.getString("reported-UUID")));
                String reason = section.getString("report-Reason");
                String time = section.getString("report-Time");
                int reportNumber = section.getInt("report-Number");
                String reportUUID = (section.getString("report-UUID"));
                int totalReports = section.getInt("total-Reports");
                ReportManager.playerReportNumber.put(reported.getUniqueId(), ReportManager.playerReportNumber.getOrDefault(reported.getUniqueId(), 0) + 1);
                Report createReport = new Report(reporter, reported, reason, time, reportNumber, UUID.fromString(reportUUID), totalReports);
                createReport.createReport();
                }
            }
        }

    public Map<UUID, UUID> getReportUUID() {
        return reportUUID;
    }


    public static Map<UUID, Integer> getPlayerTotalReports() {
        return playerTotalReports;
    }

    private void writeSection(ConfigurationSection section) {
        section.set("reported-UUID", reported.getUniqueId().toString());
        section.set("reporter-UUID", reporter.getUniqueId().toString());
        section.set("report-Reason", getReason());
        section.set("report-Number", reportNumber);
        section.set("report-Time", getDataFormatted());
        section.set("total-Reports",getReportCount(Bukkit.getPlayer(reported.getUniqueId())));
        section.set("report-UUID", String.valueOf(UUID.randomUUID()));
    }

}
