package me.imsanti.dev.managers.mode;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class StaffChat {

    private ArrayList<UUID> staffChatPlayers = new ArrayList<>();
    private final ConfigFile configFile;

    public StaffChat(ConfigFile configFile) {
        this.configFile = configFile;
    }

    public void switchStaffChat(Player player) {
        if(staffChatPlayers.contains(player.getUniqueId())) {
            disableStaffChat(player);
            return;
        }

        enableStaffChat(player);
    }

    public void enableStaffChat(Player player) {
        staffChatPlayers.add(player.getUniqueId());
        player.sendMessage(ColorUtils.color("&aHas activado el Staff Chat."));
    }

    public void disableStaffChat(Player player) {
        staffChatPlayers.remove(player.getUniqueId());
        player.sendMessage(ColorUtils.color("&cHas desactivado el Staff Chat."));
    }

    public void sendMessage(Player player, String content) {
        String format = ColorUtils.color(configFile.getConfig().getString("staff-chat.format").replace("%player%", player.getName()).replace("%message%", content));
        Bukkit.getOnlinePlayers().forEach(staff ->{
            if(staff.hasPermission("sedex.staffchat.read")) {
                staff.sendMessage(format);
            }
        });
    }

    public ArrayList<UUID> getStaffChatPlayers() {
        return staffChatPlayers;
    }

    public boolean isInStaffChat(Player player) {
        return this.getStaffChatPlayers().contains(player.getUniqueId());
    }
}
