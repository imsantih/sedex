package me.imsanti.dev.managers.objects;

import me.imsanti.dev.Sedex;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Cooldown implements Listener {

    private final HashMap<UUID, Long> cooldownBase;
    private final int cooldownTime;
    private final String cooldownName;

    private static final ArrayList<Cooldown> cooldowns = new ArrayList<>();
    public Cooldown(HashMap<UUID, Long> cooldownBase, int cooldownTime, String cooldownName) {
        this.cooldownBase = cooldownBase;
        this.cooldownTime = cooldownTime;
        this.cooldownName = cooldownName;
        cooldowns.add(this);
    }

    public void setCooldown(UUID playerUUID) {
        cooldownBase.put(playerUUID, System.currentTimeMillis() + (cooldownTime * 1000));
    }

    public boolean isInCooldown(UUID playerUUID) {
        if(!(cooldownBase.containsKey(playerUUID))) return false;

        return cooldownBase.get(playerUUID) > System.currentTimeMillis();
    }

    public int getRemainingCooldown(UUID playerUUID) {
        if(!(isInCooldown(playerUUID))) return 0;

        long cooldownNormal = cooldownBase.get(playerUUID) - System.currentTimeMillis();

        return (int) cooldownNormal / 1000;
    }

    private void removeCooldown(UUID uuid) {
        cooldownBase.remove(uuid);
    }

    public static Cooldown getCooldown(String name) {
        for(Cooldown cooldown : cooldowns) {
            if(cooldown.getCooldownName().equalsIgnoreCase(name)) return cooldown;
        }
        return null;
    }

    public String getCooldownName() {
        return cooldownName;
    }

    @EventHandler
    private void removePlayer(PlayerQuitEvent event) {
        if(!(isInCooldown(event.getPlayer().getUniqueId()))) return;


        new BukkitRunnable() {
            @Override
            public void run() {
                if(!(isInCooldown(event.getPlayer().getUniqueId()))) {
                    removeCooldown(event.getPlayer().getUniqueId());
                    cancel();
                }
            }
        }.runTaskLater(Sedex.getInstance(), 0L);
    }
}
