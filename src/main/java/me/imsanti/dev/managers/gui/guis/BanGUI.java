package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.configs.files.BanGUIFile;
import me.imsanti.dev.managers.configs.ConfigManager;
import me.imsanti.dev.managers.gui.GUIManager;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class BanGUI extends GUIManager {
    public BanGUI(Player target) {
        super(54, "BAN GUI");

        BanGUIFile banGUIFile = new BanGUIFile(new ConfigManager());
        refillBanGUI(banGUIFile, target);
    }

    private void refillBanGUI(BanGUIFile banGUIFile, Player target) {
        FileConfiguration config = banGUIFile.getBanGUIFile();
        int totalItems = config.getConfigurationSection("ban-gui").getKeys(false).size();
        for(int i = 0; i <= totalItems; i++) {
            ConfigurationSection section = config.getConfigurationSection("ban-gui." + i);
            if(section == null) continue;

            int slot = section.getInt("slot") - 1;
            if(slot > 54) slot = i;

            ArrayList<String> lore = new ArrayList();

            if(!(section.getStringList("lore") == null)) {
                for(String string : section.getStringList("lore")) {
                    lore.add(ColorUtils.color(string.replace("%player%", target.getName())));
                }

            ItemStack item = new ItemBuilder(Material.valueOf(section.getString("icon")), 1).setName(ColorUtils.color(section.getString("name"))).setLore(lore).build();
            setItem(slot, item, player -> {
                String command = section.getString("command").replace("%player%", target.getName());
                player.performCommand(command);
                });
            }
        }
    }
    public void openInventory(Player player) {
        this.openGUI(player);
        }
    }