package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.gui.GUIManager;
import org.bukkit.entity.Player;

public class GUI extends GUIManager {
    public GUI(int size, String name) {
        super(size, name);
    }

    public void openInventory(Player player) {
        openGUI(player);
    }
}
