package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.gui.EditGUIManager;
import me.imsanti.dev.managers.gui.GUIManager;
import org.bukkit.entity.Player;

public class EditGUI extends GUIManager {

    public EditGUI(EditGUIManager editGUIManager, EditGUIManager.GUIType guiType) {
        super(54, "Editing GUI" + String.valueOf(guiType));


    }

    public void openInventory(Player player) {
        openGUI(player);
    }
}
