package me.imsanti.dev.managers.gui;

import me.imsanti.dev.managers.configs.files.GUIFile;
import me.imsanti.dev.managers.gui.guis.EditGUI;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public class EditGUIManager {

    private static final HashMap<UUID, GUIType> playerEditGUI = new HashMap<>();
    private final GUIFile guiFile;

    public EditGUIManager(GUIFile guiFile) {
        this.guiFile = guiFile;
    }
    public enum GUIType {
        BANGUI,
        REPORTS,
        ONLINE_STAFF,
    }

    public void saveGUIContents(Inventory inventory, GUIType guiType) {
        int i = 0;
        FileConfiguration config = guiFile.getGUIConfig();
        ConfigurationSection section = config.createSection("guis." + guiType.toString().toUpperCase());
        for(ItemStack item : inventory.getContents()) {
            section.set(String.valueOf(i), item);
            i++;
        }

        guiFile.saveGUIFile(config);
    }

    public void openGUIEdit(Player player, String GUI) {
        if(!(Arrays.toString(GUIType.values()).contains(GUI))) return;

        final EditGUI editGUI = new EditGUI(this, GUIType.valueOf(GUI));

        loadGUIItems(GUIType.valueOf(GUI), editGUI);
        editGUI.openInventory(player);
        playerEditGUI.put(player.getUniqueId(), GUIType.valueOf(GUI));
        player.sendMessage("Ahora estas editando la GUI" + GUI);
    }

    public void loadGUIItems(GUIType guiType, EditGUI editGUI) {
        FileConfiguration config = guiFile.getGUIConfig();
        ConfigurationSection section = config.getConfigurationSection("guis." + guiType.toString().toUpperCase());
        if(section == null) return;

        for(int i = 0; i < 54; i++) {
            if(section.getItemStack(String.valueOf(i)) == null) continue;

            editGUI.setItem(i, section.getItemStack(String.valueOf(i)));
        }

    }

    public HashMap<UUID, GUIType> getPlayerEditGUI() {
        return playerEditGUI;
    }
}
