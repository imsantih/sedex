package me.imsanti.dev.managers.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class GUIManager {

    private Inventory setupGUI;
    private Map<Integer, setupGUIAction> actions;
    private UUID uuid;
    public static GUIManager instance;
    private static final Map<UUID, GUIManager> inventoriesByUUID = new HashMap<>();
    public final static Map<UUID, UUID> openInventories = new HashMap<>();

    public GUIManager(int size, String name) {
        instance = this;
        setupGUI = Bukkit.createInventory(null, size, name);
        actions = new HashMap<>();
        uuid = UUID.randomUUID();
        inventoriesByUUID.put(getUuid(), this);

    }


    public interface setupGUIAction {
        void click(Player player) throws IOException;

    }

    public void setItem(int slot, ItemStack stack, setupGUIAction action){
        setupGUI.setItem(slot, stack);
        if (action != null){
            actions.put(slot, action);
        }
    }

    public void setItem(int slot, ItemStack stack){
        setItem(slot, stack, null);
    }

    public void openGUI(Player player) {
        player.openInventory(setupGUI);
        openInventories.put(player.getUniqueId(), getUuid());
    }

    public UUID getUuid() {
        return uuid;
    }

    public Map<Integer, setupGUIAction> getActions() {
        return actions;
    }

    public static Map<UUID, GUIManager> getInventoriesByUUID() {
        return inventoriesByUUID;
    }

    public static Map<UUID, UUID> getOpenInventories() {
        return openInventories;
    }


}
