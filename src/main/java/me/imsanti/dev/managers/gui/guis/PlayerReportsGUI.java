package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.configs.ConfigManager;
import me.imsanti.dev.managers.gui.GUIManager;
import me.imsanti.dev.managers.mode.report.Report;
import me.imsanti.dev.managers.mode.report.ReportManager;
import me.imsanti.dev.utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class PlayerReportsGUI extends GUIManager {
    public PlayerReportsGUI(Player player, ReportManager reportManager) {
        super(54, "Reportes de" + player.getName());
        clearInventory(this);
        refillReports(player);
    }


    private void clearInventory(PlayerReportsGUI playerReportsGUI) {
        for(int i = 0; i < 53; i++) {
            playerReportsGUI.setItem(i, null);
        }
    }

    private void refillReports(Player player) {
        int i = 11;
        ItemStack reportItem;
        ArrayList<Report> reports = Report.getPlayerReports(player);
        Bukkit.broadcastMessage("Cuenta de reportes: " + Report.getReportCount(player));
        for(int j = 0; j <= Report.getReportCount(player); j++) {
            if (reports.get(j) == null) continue;

            Bukkit.broadcastMessage("Reporte leido: " + j);
            if(i == 16) i = 20;
            else if(i == 25) i = 29;

            ConfigManager configManager = new ConfigManager();


            Report report = reports.get(j);
            FileConfiguration config = configManager.getConfigFromFile("plugins/Sedex/reports", report.getReported().getName());

            if(config.getString("reports-" + j + ".reported-UUID") == null) {
                reportItem = new ItemStack(Material.STAINED_GLASS, 1);
            }else {
                reportItem = new ItemStack(Material.STAINED_GLASS, 1, (short) 5);
            }
            ItemMeta reportMeta = reportItem.getItemMeta();
            reportMeta.setDisplayName("Reporte " + report.getReportNumber());

            ArrayList<String> lore = new ArrayList<>();
            lore.add(ColorUtils.color(""));
            lore.add(ColorUtils.color("&eReportante: &f " + report.getReporter().getName()));
            lore.add(ColorUtils.color("&eReportado: &f " + report.getReported().getName()));
            lore.add(ColorUtils.color("&eRazon: &f " + report.getReason()));
            lore.add(ColorUtils.color("&eFecha: &f " + report.getDataFormatted()));

            reportMeta.setLore(lore);

            reportItem.setItemMeta(reportMeta);


            setItem(i, reportItem, player4 -> {
                report.saveReport();
                player.closeInventory();

            });
            i++;
            }
        }


    public void openInventory(Player player) {
        this.openGUI(player);
    }
}
