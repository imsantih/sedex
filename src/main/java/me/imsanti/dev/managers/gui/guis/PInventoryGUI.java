package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.gui.GUIManager;
import me.imsanti.dev.managers.mode.report.Report;
import me.imsanti.dev.managers.mode.report.ReportManager;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;

public class PInventoryGUI extends GUIManager {
    public PInventoryGUI(Player player, ReportManager reportManager) {
        super(54, ColorUtils.color("&b&lInventario de " + player.getName()));
        if(!(player.isOnline())) return;

        int i = 0;

        final ItemStack DECORATION_ITEM = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);
        final ItemStack REPORTS_ITEM = new ItemBuilder(Material.BLAZE_POWDER, 1).setName(ColorUtils.color("&aSubir Reportes")).build();

            setGUIContents(player);

            setItem(40, DECORATION_ITEM, player2 -> {
                player2.sendMessage("has clickeado el cristal :D");
            });

        setItem(47, REPORTS_ITEM, player2 -> {
            if(Report.getReportCount(player) == 0) return;

            player2.closeInventory();
            player2.sendMessage("Subiendo el reporte...");
            player2.sendMessage(reportManager.uploadReports(player));

        });

        }

    private void setGUIContents(Player target) {
        int i = 0;

        for(int j = 0; j < target.getInventory().getContents().length; j++) {
            setItem(j, null);
            j++;
            target.updateInventory();
        }

        for(ItemStack item : target.getInventory()) {
            setItem(i, item, player2 -> {
            });
            i++;
        }
    }

    public void openPlayerInventory(Player player) {
        this.openGUI(player);
    }
}
