package me.imsanti.dev.managers.gui;

import me.imsanti.dev.managers.configs.files.ConfigFile;
import me.imsanti.dev.managers.configs.files.GUIFile;
import me.imsanti.dev.managers.gui.EditGUIManager.GUIType;
import me.imsanti.dev.managers.gui.guis.GUI;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;

public class GUIUtils {

    private final EditGUIManager editGUIManager;
    private final Player player;
    private static final Map<GUIType, Integer> specialSlotsAmount = new HashMap<>();
    private final ConfigFile configFile;
    private final GUIFile guiFile;
    private final ArrayList<GUI> pages = new ArrayList<>();
    private boolean isFirstTime;
    HashMap<UUID, Integer> playerPage = new HashMap<>();

    public GUIUtils(EditGUIManager editGUIManager, Player player, GUIFile guiFile, ConfigFile configFile) {
        this.editGUIManager = editGUIManager;
        this.player = player;
        this.guiFile = guiFile;
        this.configFile = configFile;
    }

    public void createGUI(EditGUIManager.GUIType guiType, Player player) {

        createGUI(player, guiType);
    }


    public void createGUI(Player player, EditGUIManager.GUIType guiType) {
        int currentPage;
        int neededPages;

        switch(guiType) {
            case ONLINE_STAFF:
                ArrayList<Player> staffs = SUtils.getPermissionPlayers("sedex.staff");
                if(!(staffs.size() == 0)) {
                    ConfigurationSection section = guiFile.getGUIConfig().getConfigurationSection("guis." + guiType.toString().toUpperCase());

                    int staffSlots = specialSlotsAmount.get(GUIType.ONLINE_STAFF);
                    if(staffSlots > staffs.size()) {
                        neededPages = 1;
                    }else {
                        neededPages = Math.max(staffs.size(), staffSlots) / Math.min(staffs.size(), staffSlots);
                    }

                    movePage(player, true, createBlankPage(staffs, guiType, "{STAFFITEM}", section, neededPages), neededPages);
                    break;
            }

            default:
                break;
        }
    }
    public GUI refillGUI(EditGUIManager.GUIType guiType, Player executor)  {

        FileConfiguration config = guiFile.getGUIConfig();
        FileConfiguration mainConfig = configFile.getConfig();
        ConfigurationSection section = config.getConfigurationSection("guis." + guiType.toString().toUpperCase());
        switch(guiType) {

            case ONLINE_STAFF:
                ArrayList<Player> staffs = new ArrayList<>();
                int staffSlots = 0;
                int pagesToCreate = 0;
                for(int i = 0; i < 54; i++) {
                    if(section.getItemStack(String.valueOf(i)) == null) continue;
                    ItemStack item = section.getItemStack(String.valueOf(i));
                }
//
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if(player.hasPermission("sedex.staff")) {
                        staffs.add(player);
                    }
                }

               if(specialSlotsAmount.get(GUIType.ONLINE_STAFF) < staffs.size()) {
                   pagesToCreate = 1;
                }else {
                    pagesToCreate = Math.max(staffs.size(), staffSlots) / Math.min(staffs.size(), specialSlotsAmount.get(GUIType.ONLINE_STAFF));
                }


            default:
                break;
        }
        mainConfig = null;
        config = null;


        return null;

    }



    public ItemStack addItems(ItemStack item, int maxValueFromConfig) {
        int newAmount = 0;

        while(newAmount < item.getAmount() + maxValueFromConfig) {
            newAmount++;
        }

        item.setAmount(newAmount - item.getAmount());

        return item;
    }

    private ArrayList<GUI> createBlankPage(ArrayList<Player> list, EditGUIManager.GUIType guiType, String specialVariableName, ConfigurationSection section, int pagesToCreate) {
        ArrayList<GUI> guis = new ArrayList<>();
        for(int i = 0; i < pagesToCreate; i++) {
            FileConfiguration mainConfig = configFile.getConfig();

            GUI blankGui = new GUI(54, guiType.toString());
            HashMap<ItemStack, Integer> pendingItems = new HashMap<>();
            guis.add(blankGui);

            for(int j = 0; j < 54; j++) {
                if(section.getItemStack(String.valueOf(j)) == null) continue;
                ItemStack item = section.getItemStack(String.valueOf(j));

                if(item.hasItemMeta() && item.getItemMeta().getDisplayName().equalsIgnoreCase(specialVariableName.toUpperCase())) {
                    pendingItems.put(item, j);
                    Bukkit.broadcastMessage(String.valueOf(j));
                }

                if(item.hasItemMeta() && item.getItemMeta().getDisplayName().equalsIgnoreCase("{NEXTPAGE}")) {
                    blankGui.setItem(j, SUtils.readItem(mainConfig.getConfigurationSection("mode-menus.next-Page")), player2 -> {
                        movePage(player2, true, guis, pagesToCreate);
                    });
                    continue;
                }

                if(item.hasItemMeta() && item.getItemMeta().getDisplayName().equalsIgnoreCase("{PREVIUSPAGE}")) {
                    blankGui.setItem(j, SUtils.readItem(mainConfig.getConfigurationSection("mode-menus.previus-Page")), player2 -> {
                        movePage(player2, false, guis, pagesToCreate);
                    });

                    continue;
                }

                blankGui.setItem(j, section.getItemStack(String.valueOf(j)));

            }
            switch(guiType) {
                case ONLINE_STAFF:
                    ListIterator<Player> listIterator = list.listIterator();
                    for(ItemStack item : pendingItems.keySet()) {
                        if(!(listIterator.hasNext())) return guis;

                        blankGui.setItem(pendingItems.get(item), createStaffItem(mainConfig, listIterator, list));
                        Bukkit.broadcastMessage(String.valueOf(pendingItems.get(item)));

                        if(listIterator.nextIndex() -1 == -1) {
                            list.remove(0);
                            continue;
                        }

                        list.remove(listIterator.nextIndex() - 1);
                    }
                    break;

                default:
                    break;
            }
        }
        return guis;
    }

    private ItemStack createStaffItem(FileConfiguration mainConfig, ListIterator list, ArrayList<Player> staffs) {

        int value = 0;

        ItemMeta itemMeta = SUtils.readItemMenu(mainConfig.getConfigurationSection("staffList.staff-Item"));
        ItemStack staffSkull = new ItemStack(Material.SKULL_ITEM,  1, (short) 3);
        SkullMeta skullMeta = (SkullMeta) staffSkull.getItemMeta();

        if(!(list.nextIndex() - 1 == -1)) {
            value = list.nextIndex() - 1;
        }

        skullMeta.setOwner(staffs.get(value).getName());
        skullMeta.setLore(itemMeta.getLore());
        skullMeta.setDisplayName(itemMeta.getDisplayName().replace("%player%", staffs.get(value).getName()));
        staffSkull.setItemMeta(skullMeta);

        return staffSkull;

        }

    public void cacheAvaibleSlots() {
        FileConfiguration config = guiFile.getGUIConfig();
        Map<GUIType, String> specialName = new HashMap<>();
        int slots;
        specialName.put(GUIType.ONLINE_STAFF, "{STAFFITEM}");

        for(GUIType guiType : GUIType.values()) {
            slots = 0;
            ConfigurationSection section = config.getConfigurationSection("guis." + guiType.toString().toUpperCase());
            if(section == null) continue;

            for(String key : section.getKeys(false)) {
                ItemStack itemStack = section.getItemStack(String.valueOf(key));
                if(itemStack == null) continue;
                if(itemStack.hasItemMeta() && itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(specialName.get(guiType))) {
                    slots++;
                }
            }

            specialSlotsAmount.put(guiType, slots);
        }

//        specialName.clear();
//        specialName = null;
    }


    public void movePage(Player player, boolean up, ArrayList<GUI> guis, int totalPages) {
        playerPage.putIfAbsent(player.getUniqueId(), 0);

        Bukkit.broadcastMessage("Despues de agregarlo: " + playerPage.get(player.getUniqueId()));
        if(totalPages < playerPage.get(player.getUniqueId()) - 1 ) return;
        if(playerPage.get(player.getUniqueId()) > totalPages) return;

        if(up) {
            int currentPage = playerPage.get(player.getUniqueId());
            if(guis.get(currentPage) == null) {
                playerPage.remove(player.getUniqueId());
                return;
            }

            guis.get(currentPage).openInventory(player);
            playerPage.put(player.getUniqueId(), playerPage.get(player.getUniqueId()) + 1);
            Bukkit.broadcastMessage(String.valueOf(playerPage.get(player.getUniqueId())));
            return;

        }

        if(playerPage.containsKey(player.getUniqueId())) playerPage.put(player.getUniqueId(), playerPage.get(player.getUniqueId()) - 2);

        int currentPage = playerPage.get(player.getUniqueId());

        if(guis.get(currentPage) == null) {
            playerPage.remove(player.getUniqueId());
            return;
        }

        Bukkit.broadcastMessage(String.valueOf(playerPage.get(player.getUniqueId())));
        guis.get(currentPage).openInventory(player);
    }

    public int getCurrentPage(Player player) {
        return playerPage.get(player.getUniqueId());
    }

    public HashMap<UUID, Integer> getPlayerPage() {
        return playerPage;
    }
}

