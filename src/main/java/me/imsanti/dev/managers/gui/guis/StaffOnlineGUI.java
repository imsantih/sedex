package me.imsanti.dev.managers.gui.guis;

import me.imsanti.dev.managers.gui.GUIManager;
import me.imsanti.dev.utils.ColorUtils;
import me.imsanti.dev.utils.SUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

public class StaffOnlineGUI extends GUIManager {
    public StaffOnlineGUI() {
        super(54, "Lista de Staff Online");

        int slot = 11;

        for(Player target : Bukkit.getOnlinePlayers()) {
            if(slot == 16) slot = 20;
            else if(slot == 25) slot = 29;

            if(!(target.hasPermission("sedex.staff"))) return;

            ItemStack staffSkull = new ItemStack(Material.SKULL_ITEM,  1, (short) 3);
            SkullMeta skullMeta = (SkullMeta) staffSkull.getItemMeta();
            skullMeta.setOwner(target.getName());
            skullMeta.setDisplayName(target.getName());

            skullMeta.setLore(createLore(target));
            staffSkull.setItemMeta(skullMeta);

            setItem(slot, staffSkull);
            slot++;

        }
    }

    public void openInventory(Player player) {
        this.openGUI(player);
    }

    private ArrayList<String> createLore(Player player) {

        ArrayList<String> lore = new ArrayList<>();
        lore.add(ColorUtils.color("&e"));
        lore.add(ColorUtils.color("&bNick: &f "  + player.getName()));
        lore.add(ColorUtils.color("&bGamemode: &f "  + player.getGameMode().toString()));
        lore.add(ColorUtils.color("&bLocation: &f " + SUtils.formatLocation(player)));
        lore.add(ColorUtils.color("&bHealth: &f " + player.getHealth()));
        lore.add(ColorUtils.color("&bHunger: &f " + player.getFoodLevel()));

        return lore;

    }
}
