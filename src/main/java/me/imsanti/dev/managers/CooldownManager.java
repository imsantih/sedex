package me.imsanti.dev.managers;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.listeners.cooldowns.ChatDelay;
import me.imsanti.dev.managers.objects.Cooldown;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import java.util.HashMap;
import java.util.UUID;

public class CooldownManager {

    private final HashMap<UUID, Long> chatCooldown = new HashMap();

    private final int CHAT_COOLDOWN_TIME = 10;
    public static boolean chatCooldownEnabled;

    public void enableChatCooldown() {
        if(chatCooldownEnabled) return;
        Bukkit.broadcastMessage("Chat Cooldown activo!");
        Cooldown chatCooldown = new Cooldown(getChatCooldown(), CHAT_COOLDOWN_TIME, "chatCooldown");
        Bukkit.getPluginManager().registerEvents(new ChatDelay(chatCooldown), Sedex.getInstance());
        chatCooldownEnabled = true;
    }

    public void disableChatCooldown() {
        if(!(chatCooldownEnabled)) return;

        Bukkit.broadcastMessage("cooldown desactivado");

        final ChatDelay chatDelay = new ChatDelay(Cooldown.getCooldown("chatCooldown"));
        chatCooldownEnabled = false;
        chatDelay.shutdownMe();
    }

    public void toggleChatCooldown() {
        if(chatCooldownEnabled) {
            disableChatCooldown();
        }else enableChatCooldown();
    }


    public HashMap<UUID, Long> getChatCooldown() {
        return chatCooldown;
    }
}
