package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

public class DataFile {

    private final static ConfigManager configManager = new ConfigManager();
    private final Player player;
    public DataFile(ConfigManager configManager, Player player) {
        this.player = player;

    }

    final static String PATH_VALUE = Sedex.getInstance().getDataFolder() + "/players";

    public boolean createDataFile() {
        return configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), player.getName());
    }

    public void saveDataFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public static FileConfiguration getDataFile(Player player) {
        return configManager.getConfigFromFile(String.valueOf(PATH_VALUE), player.getName() + ".yml");
    }

    public File getConfigFile() {
        return new File(PATH_VALUE, player.getName() + ".yml");
    }

}