package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class ConfigFile {

    public ConfigFile(ConfigManager configManager) {
        this.configManager = configManager;
    }

    private final ConfigManager configManager;

    public boolean createConfigFile() {
        if(configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), "config")) return true;

        return false;
    }

    public void saveConfigFile(FileConfiguration config) {
        configManager.saveConfigFile(getConfigFile(), config);
    }

    public FileConfiguration getConfig() {
        return configManager.getConfigFromFile(String.valueOf(Sedex.getInstance().getDataFolder()), "config");
    }

    public File getConfigFile() {
        return new File(Sedex.getInstance().getDataFolder(), "config.yml");
    }
}