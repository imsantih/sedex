package me.imsanti.dev.managers.configs;

import me.imsanti.dev.Sedex;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class ConfigManager {

    private File file;
    private FileConfiguration fileconfig;

    public void saveConfigFile(File file, FileConfiguration fileConfiguration) {
        try {
            fileConfiguration.save(file);
            Bukkit.getConsoleSender().sendMessage("Configuration has been saved!");
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage("We can't reload your configuration!");
            e.printStackTrace();
        }
    }

    public boolean createConfigFile(String location, String filename) {
        file = new File(location, filename + ".yml");
        if (!file.exists()) {
            try {
                Sedex.getInstance().saveResource(filename + ".yml", false);
                return true;
            } catch(Exception exception) {
                exception.printStackTrace();
                return false;
            }
        }

        fileconfig = new YamlConfiguration();
        try {
            fileconfig.load(file);
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage("Error in trying to load the configuration!");
            e.printStackTrace();

        }

        return false;
    }


    public FileConfiguration getConfigFromFile(String location, String fileName) {
        File file = new File(location, fileName + ".yml");
        return YamlConfiguration.loadConfiguration(file);
    }
}