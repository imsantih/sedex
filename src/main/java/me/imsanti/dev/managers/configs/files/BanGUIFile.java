package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class BanGUIFile {

    private final ConfigManager configManager;


    public BanGUIFile(ConfigManager configManager) {
        this.configManager = configManager;
    }

    public boolean createBanGUIFile() {
        if(configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), "bangui")) {
            return true;
        }
        return false;
    }

    public void saveBanGUIFile(FileConfiguration config) {
        configManager.saveConfigFile(getLocationsFile(), config);
    }

    public FileConfiguration getBanGUIFile() {
        return configManager.getConfigFromFile(String.valueOf(Sedex.getInstance().getDataFolder()), "bangui");
    }

    public File getLocationsFile() {
        return new File(Sedex.getInstance().getDataFolder(), "bangui.yml");
    }
}
