package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class MessagesFF {

    private final ConfigManager configManager;


    public MessagesFF(ConfigManager configManager) {
        this.configManager = configManager;
    }

    public boolean createMessages() {
        if(configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), "messages")) return true;

        return false;
    }

    public void saveMessages(FileConfiguration config) {
        configManager.saveConfigFile(getMessagesFile(), config);
    }

    public FileConfiguration getMessages() {
        return configManager.getConfigFromFile(String.valueOf(Sedex.getInstance().getDataFolder()), "messages");
    }

    public File getMessagesFile() {
        return new File(Sedex.getInstance().getDataFolder(), "messages.yml");
    }
}
