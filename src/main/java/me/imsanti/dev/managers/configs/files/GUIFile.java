package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class GUIFile {

    private final ConfigManager configManager;

    public GUIFile(ConfigManager configManager) {
        this.configManager = configManager;
    }

    public boolean createGUIFile() {
        if(configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), "guis")) return true;

        return false;
    }

    public void saveGUIFile(FileConfiguration config) {
        configManager.saveConfigFile(getGUIFile(), config);
    }

    public FileConfiguration getGUIConfig() {
        return configManager.getConfigFromFile(String.valueOf(Sedex.getInstance().getDataFolder()), "guis");
    }

    public File getGUIFile() {
        return new File(Sedex.getInstance().getDataFolder(), "guis.yml");
    }
}
