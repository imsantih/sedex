package me.imsanti.dev.managers.configs.files;

import me.imsanti.dev.Sedex;
import me.imsanti.dev.managers.configs.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;

public class LocationsFile {

    private final ConfigManager configManager;


    public LocationsFile(ConfigManager configManager) {
        this.configManager = configManager;
    }

    public boolean createLocationsFile() {
        if(configManager.createConfigFile(String.valueOf(Sedex.getInstance().getDataFolder()), "locations")) return true;

        return false;
    }

    public void saveLocationsFile(FileConfiguration config) {
        configManager.saveConfigFile(getLocationsFile(), config);
    }

    public FileConfiguration getLocationsConfg() {
        return configManager.getConfigFromFile(String.valueOf(Sedex.getInstance().getDataFolder()), "locations");
    }

    public File getLocationsFile() {
        return new File(Sedex.getInstance().getDataFolder(), "locations.yml");
    }
}
