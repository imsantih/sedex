package me.imsanti.dev.managers;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class ChatManager {

    private boolean isChatEnabled = true;

    public void toggleChat(CommandSender sender) {
        if(isChatEnabled) {
            Bukkit.broadcastMessage(sender.getName() + "ha desactivado el chat");
            isChatEnabled = false;
            return;
        }
        Bukkit.broadcastMessage(sender.getName() + "ha activado el chat");
        isChatEnabled = true;

    }

    public void enableChat(CommandSender sender) {
        if(isChatEnabled) return;

        Bukkit.broadcastMessage(sender.getName() + "ha activado el chat");
        isChatEnabled = true;
    }

    public void disableChat(CommandSender sender) {
        if(!(isChatEnabled)) return;

        Bukkit.broadcastMessage(sender.getName() + "ha desactivado el chat");
        isChatEnabled = false;
    }

    public void clearChat(CommandSender sender) {
        for(int i = 0; i < 250; i++) {
            Bukkit.broadcastMessage("");
        }

        Bukkit.broadcastMessage("El chat fue limpiado por " + sender.getName());
    }

    public boolean isChatEnabled() {
        return isChatEnabled;
    }
}
