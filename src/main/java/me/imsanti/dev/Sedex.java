package me.imsanti.dev;

import me.imsanti.dev.commands.chat.ChatCommand;
import me.imsanti.dev.commands.gui.GUICommand;
import me.imsanti.dev.commands.mode.*;
import me.imsanti.dev.commands.notes.NotesCommand;
import me.imsanti.dev.commands.reports.ReportCommand;
import me.imsanti.dev.listeners.*;
import me.imsanti.dev.managers.ChatManager;
import me.imsanti.dev.managers.configs.*;
import me.imsanti.dev.managers.configs.files.*;
import me.imsanti.dev.managers.gui.EditGUIManager;
import me.imsanti.dev.managers.gui.GUIUtils;
import me.imsanti.dev.managers.mode.*;
import me.imsanti.dev.managers.mode.notes.NotesManager;
import me.imsanti.dev.managers.mode.report.ReportManager;
import me.imsanti.dev.utils.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Arrays;

public final class Sedex extends JavaPlugin {

    public static Sedex instance;

    private final ModeManager modeManager;
    private final  StaffChat staffChat;
    private final MessageUtils messageUtils;
    private final MessagesFF messagesFF;
    private final ModeUtils modeUtils;
    private final Freeze freeze;
    private final RandomTeleport randomTeleport;
    private final ConfigManager configManager;
    private final LocationsFile locationsFile;
    private final ReportManager reportManager;
    private final ChatManager chatManager;
    private final BanGUIFile banGUIFile;
    private final ConfigFile configFile;
    private final Vanish vanish;
    private final GUIFile guiFile;
    private final EditGUIManager editGUIManager;
    private final NotesManager notesManager;
    private final GUIUtils guiUtils;

    public Sedex() {
        this.configManager = new ConfigManager();
        this.configFile = new ConfigFile(configManager);
        this.messagesFF = new MessagesFF(configManager);
        this.chatManager = new ChatManager();
        this.randomTeleport = new RandomTeleport(configFile, messagesFF);
        this.vanish = new Vanish();
        this.messageUtils = new MessageUtils(messagesFF);

        this.locationsFile = new LocationsFile(configManager);
        this.staffChat = new StaffChat(configFile);
        this.freeze = new Freeze(locationsFile, configFile);
        this.modeManager = new ModeManager(messageUtils, configFile, randomTeleport, freeze, vanish);
        this.modeUtils = new ModeUtils(modeManager);
        this.reportManager = new ReportManager();
        this.banGUIFile = new BanGUIFile(configManager);
        this.guiFile = new GUIFile(configManager);
        this.editGUIManager = new EditGUIManager(guiFile);
        this.notesManager = new NotesManager();
        this.guiUtils = new GUIUtils(editGUIManager, null, guiFile, configFile);

        instance = this;
    }

    @Override
    public void onEnable() {
        try {
            loadPlugin();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
//        try {
//           // loadPluginFiles();
//        } catch (IOException exception) {
//            exception.printStackTrace();
//        }
       // Report.readReports();

        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        vanish.getHidedPlayers().clear();
        freeze.banFreezedPlayers();
        freeze.getFreezedPlayers().clear();
        // Plugin shutdown logic
    }

    private void registerEvents() {
        //Chat Cooldown
        //Bukkit.getPluginManager().registerEvents(Cooldown.getCooldown("chatCooldown"), this);

        Bukkit.getPluginManager().registerEvents(new EditGUIHandler(editGUIManager), this);
        Bukkit.getPluginManager().registerEvents(new HungerListener(modeManager, configFile), this);
        Bukkit.getPluginManager().registerEvents(new DamageListener(modeManager, configFile), this);
        Bukkit.getPluginManager().registerEvents(new InventoryClickListener(modeUtils, editGUIManager, guiUtils), this);
        Bukkit.getPluginManager().registerEvents(new InteractListener(freeze, modeManager, configFile), this);
        Bukkit.getPluginManager().registerEvents(new WorldChangeListener(freeze, locationsFile, vanish), this);
        Bukkit.getPluginManager().registerEvents(new MoveListener(freeze), this);
        Bukkit.getPluginManager().registerEvents(new ConnectionListener(freeze, vanish, configFile, modeManager, messagesFF), this);
        Bukkit.getPluginManager().registerEvents(new ChatListener(staffChat, freeze, chatManager), this);
        Bukkit.getPluginManager().registerEvents(new DropListener(modeUtils, modeManager), this);

        if(freeze.getFreezeMode().equalsIgnoreCase("LOCATION")) Bukkit.getPluginManager().registerEvents(new MoveListener(freeze), this);
    }
    public static Sedex getInstance() {
        return instance;
    }


    private void loadPlugin() throws IOException {
        final long startTime = System.nanoTime();


        Bukkit.getConsoleSender().sendMessage("   _____ ______ _____  ________   __ ");
        Bukkit.getConsoleSender().sendMessage("  / ____|  ____|  __ \\|  ____\\ \\ / / ");
        Bukkit.getConsoleSender().sendMessage(" | (___ | |__  | |  | | |__   \\ V /  ");
        Bukkit.getConsoleSender().sendMessage("  \\___ \\|  __| | |  | |  __|   > <  ");
        Bukkit.getConsoleSender().sendMessage("  ____) | |____| |__| | |____ / . \\  ");
        Bukkit.getConsoleSender().sendMessage(" |_____/|______|_____/|______/_/ \\_\\ ");
        Bukkit.getConsoleSender().sendMessage(" ");

        if(locationsFile.createLocationsFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created Locations File (locations.yml)");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded Locations File (locations.yml)");
        }

        if(configFile.createConfigFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created Config File (config.yml)");
            return;
        }

         Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded Config File (config.yml)");

        if(guiFile.createGUIFile()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created GUI File (guis.yml)");
            return;
        }

        Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded GUI File (guis.yml)");

        if(messagesFF.createMessages()) {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Created Messages File (messages.yml)");
        }else {
            Bukkit.getConsoleSender().sendMessage("[Config-Manager] Loaded Messages File (messages.yml)");
        }

        Bukkit.getConsoleSender().sendMessage(" ");


//        ConfigUpdater.update(this, "config.yml", configFile.getConfigFile(), Arrays.asList("mode-items"));
//        ConfigUpdater.update(this, "bangui.yml", banGUIFile.getLocationsFile(), Arrays.asList("mode-items"));
//        ConfigUpdater.update(this, "locations.yml", locationsFile.getLocationsFile(), Arrays.asList("mode-items"));

        Bukkit.getConsoleSender().sendMessage(" ");
        modeManager.loadItems();
        Bukkit.getConsoleSender().sendMessage(" ");
        registerEvents();
        Bukkit.getConsoleSender().sendMessage("[Event-Manager] Registered " + HandlerList.getRegisteredListeners(this).size() + " events successfully.");
        Bukkit.getConsoleSender().sendMessage(" ");
        registerCommands();
        guiUtils.cacheAvaibleSlots();
        final long end = System.nanoTime();
        String finalTime = String.valueOf((end - startTime) / 1000000);
        Bukkit.getConsoleSender().sendMessage("[Sedex] Sedex plugin took " + finalTime + " ms to load.");
    }

    private void registerCommands() {

        getCommand("chat").setExecutor(new ChatCommand(chatManager));
        getCommand("gui").setExecutor(new GUICommand());
        getCommand("freeze").setExecutor(new FreezeCommand(freeze, configFile));
        getCommand("randomteleport").setExecutor(new RandomTeleportCommand(randomTeleport));
        getCommand("staffchat").setExecutor(new StaffChatCommand(staffChat));
        getCommand("staff").setExecutor(new StaffCommand(modeManager, messagesFF));
        getCommand("vanish").setExecutor(new VanishCommand(vanish, messagesFF));
        getCommand("notes").setExecutor(new NotesCommand(notesManager));
        getCommand("report").setExecutor(new ReportCommand());
    }

}

