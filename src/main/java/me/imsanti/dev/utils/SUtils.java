package me.imsanti.dev.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class SUtils {

    public static void writeLocation(Location location, ConfigurationSection section) {
        section.set("x", location.getX());
        section.set("y", location.getY());
        section.set("z", location.getZ());
        section.set("yaw", location.getYaw());
        section.set("pitch", location.getPitch());
        section.set("world", location.getWorld().getName());
    }

    public static Location readLocation(ConfigurationSection section) {
        return new Location(Bukkit.getWorld(String.valueOf(section.get("world"))), section.getDouble("x"), section.getDouble("y"), section.getDouble("z"), (float) section.getDouble("yaw"), (float) section.getDouble("pitch"));
    }

    public static String formatLocation(Player player) {
        String x = String.valueOf(Math.round(player.getLocation().getX()));
        String y = String.valueOf(Math.round(player.getLocation().getY()));
        String z = String.valueOf(Math.round(player.getLocation().getZ()));
        String worldName = player.getWorld().getName();

        return x + "," + " " + y + "," + " " + z + "," + " " + worldName;
    }

    public static ItemStack readItem(ConfigurationSection section) {
        ItemStack item = new ItemStack(Material.valueOf(section.getString("item")), 1);
        ItemMeta itemMeta = item.getItemMeta();
        if(!(section.getStringList("lore") == null)) {
            ArrayList<String> lore = new ArrayList<>();
            for(String string : section.getStringList("lore")) {
                lore.add(ColorUtils.color(string));
            }

            itemMeta.setLore(lore);
            lore.clear();
        }

        if(!(section.getString("name") == null)) {
            itemMeta.setDisplayName(ColorUtils.color(section.getString("name")));
        }

        item.setItemMeta(itemMeta);
        return item;
    }


    public static ItemMeta readItemMenu(ConfigurationSection section) {
        ItemStack nullItem = new ItemStack(Material.STONE, 1);
        ItemMeta itemMeta = nullItem.getItemMeta();
        if(!(section.getStringList("lore") == null)) {
            ArrayList<String> lore = new ArrayList<>();
            for(String string : section.getStringList("lore")) {
                lore.add(ColorUtils.color(string));
            }

            itemMeta.setLore(lore);
            lore.clear();
        }

        if(!(section.getString("name") == null)) {
            itemMeta.setDisplayName(ColorUtils.color(section.getString("name")));
        }

        return itemMeta;
    }

    public static ArrayList<Player> getPermissionPlayers(String permisssion) {
        ArrayList<Player> permissionPlayers = new ArrayList<>();
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(!(player.hasPermission(permisssion))) continue;

            permissionPlayers.add(player);
        }

        Bukkit.broadcastMessage(permissionPlayers.toString());

        return permissionPlayers;
    }


    private static final Pattern PATTERN = Pattern.compile("\"?( |$)(?=(([^\"]*\"){2})*[^\"]*$)\"?");

    public static String[] getSpacesInArgs(String[] arguments) {
        return PATTERN.split(String.join(" ", arguments).replaceAll("^\"", ""));
    }

    public static String toText(String[] split) {
        StringBuilder finalMessage = new StringBuilder();
        for(String string : split) {
            if(string.startsWith("-w") || string.startsWith("-p") || string.startsWith("c:") || string.startsWith("-r") || string.startsWith("-c")) continue;
            finalMessage.append(string).append(" ");

        }
        return finalMessage.toString().trim();
    }
}
