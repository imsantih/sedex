package me.imsanti.dev.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class Hastebin {

    public static String createHasteBin(ArrayList<String> array) throws MalformedURLException, IOException {
        String content = null;
        URL url = new URL("https://hasteb.in/documents");
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;
        for(String string : array) {
            if(string.equals("null")) continue;

            http.addRequestProperty("data", string);
            if(content == null) content = "";

            content = content + "\n" + string;
        }
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        try (OutputStream out = http.getOutputStream()) {
            out.write(content.getBytes());
            out.flush();
        }
        InputStream in = new BufferedInputStream(http.getInputStream());
        StringBuilder entirePage;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {

            entirePage = new StringBuilder();
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                entirePage.append(inputLine);
            }
            reader.close();
        }
        if (!(entirePage.toString().contains("\"key\":\""))) {
            return "UNKNOWN";
        }
        return "https://hasteb.in/"+entirePage.toString().split("\"key\":\"")[1].split("\",")[0].replace("\"", "").replace("}", "");
    }
}
