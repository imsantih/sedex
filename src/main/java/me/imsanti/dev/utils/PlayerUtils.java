package me.imsanti.dev.utils;

import org.bukkit.Bukkit;

public class PlayerUtils {

    public static boolean isValidPlayer(String player) {
        return Bukkit.getPlayer(player) != null && Bukkit.getPlayer(player).isOnline();
    }

    public static void sendGlobalMessage(String permission, String message) {
        Bukkit.getOnlinePlayers().forEach(player -> {
            if(player.hasPermission(permission)) {
                player.sendMessage(ColorUtils.color(message));
            }
        });
    }
}
