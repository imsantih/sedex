package me.imsanti.dev.utils;

import me.imsanti.dev.managers.configs.files.MessagesFF;
import me.imsanti.dev.managers.mode.ModeUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MessageUtils {

    private static MessagesFF messagesFF = null;

    public MessageUtils(MessagesFF messagesFF) {
        MessageUtils.messagesFF = messagesFF;
    }

    public void sendMessage(Player player ,String message) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static void sendAllStaffMessage(String message) {
        for(Player player : Bukkit.getOnlinePlayers()) {
            if(!(ModeUtils.isStaff(player))) return;

            player.sendMessage(ColorUtils.color(message));
        }
    }

    public static void sendNoPermission(Player player) {
        player.sendMessage(ColorUtils.color(messagesFF.getMessages().getString("general.no-permission")));
    }

    public static void sendNoConsole() {
        Bukkit.getConsoleSender().sendMessage(ColorUtils.color(messagesFF.getMessages().getString("general.no-console")));
    }
}
