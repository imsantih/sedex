package me.imsanti.dev.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String color(String text) {
        try {
            return ChatColor.translateAlternateColorCodes('&', text);
        }catch(NullPointerException exception) {
            return text.replace("&", "");
        }

    }

    public static String removeColor(String text) {
        return ChatColor.stripColor(text);
    }
}
